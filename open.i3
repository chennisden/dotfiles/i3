# Opening programs

bindsym $mod+Return exec alacritty
bindsym $mod+q kill

# dmenu
bindsym $mod+d exec --no-startup-id dmenu_run

# flameshot (screenshots)
bindsym --release Print exec flameshot gui
bindsym --release Shift+Print exec flameshot full -c

# shutdown
bindsym $mod+Shift+s exec shutdown now

# launcher mode
mode "launcher" {	
	bindsym f exec firefox; mode "default"
	# school profile of firefox
	bindsym s exec school; mode "default"
	bindsym c exec chromium; mode "default"
	bindsym b exec btd; mode "default"
	bindsym d exec discord; mode "default"
	bindsym z exec zathura; mode "default"
	bindsym Escape mode "default"
}

bindsym $mod+o mode "launcher"
